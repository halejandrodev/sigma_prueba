<?php
    require_once("departamentos.php");
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sigma - Prueba</title>
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">

    <!-- Css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/css/bootstrap.min.css" integrity="sha384-VCmXjywReHh4PwowAiWNagnWcLhlEJLA5buUprzK8rxFgeH0kww/aWY76TfkUoSX" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/index.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">

    <!-- Js -->
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/js/bootstrap.min.js" integrity="sha384-XEerZL0cuoUbHE4nZReLT7nx9gQrQreJekYhJD9WNWhH8nEW+0c5qq7aIo2Wl30J" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>


</head>

<body>

    <div class="container">

        <div class="row logo">
            <div class="col-md-12">
                <img src="assets/img/sigma-logo.png" alt="sigma-logo.png">
            </div>
        </div>

        <div class="row titulo">
            <div class="col-md-12">
                <h1>Prueba de desarrollo Sigma</h1>
            </div>
        </div>

        <div class="row parrafo">
            <div class="col-md-12">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type
                    specimen book.
                </p>
            </div>
        </div>

        <div class="row centroApp">
            <div class="col-md-6">
                <img src="assets/img/sigma-image.png" class="img-fluid" alt="sigma-image.png">
            </div>
            <div class="col-md-6">
                <div class="formulario">
                    <div class="form-group">
                        <label for="departamento">Departamento*</label>
                        <select name="departamento" id="departamento" class="selects">
                            <option value="@">---</option>
                        <?php 
                            foreach($departamentos AS $departamento){
                        ?>
                            <option value="<?=$departamento?>"><?=$departamento?></option>
                        <?php
                            }
                        ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="ciudad">Ciudad*</label>
                        <select name="ciudad" id="ciudad" class="selects">
                            <option value="@">---</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nombre">Nombre*</label>
                        <input type="text" name="nombre" id="nombre" class="inputs" maxlength="50" placeholder="Pepito de Jesús">
                    </div>
                    <div class="form-group">
                        <label for="correo">Correo*</label>
                        <input type="mail" name="correo" id="correo" class="inputs" maxlength="30" placeholder="pepitodejesus@gmail.com">
                    </div>
                    <div class="content-btn">
                        <button id="enviar" class="btn btn-lg button">ENVIAR</button>
                    </div>

                </div>
            </div>
        </div>


    </div>
    <script src="assets/index.js"></script>
</body>

</html>