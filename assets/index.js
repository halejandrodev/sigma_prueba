$(document).ready(function() {

    $('#ciudad').attr('disabled', 'disable');
});

$('#departamento').change(function() {
    $.ajax({
        url: "ciudades.php",
        method: "POST",
        dataType: "json",
        data: {
            departamento: $('#departamento').val(),
        },
        success: function(response) {
            $('#ciudad').removeAttr('disabled');
            $("#ciudad").html(response['resultado']);
        }
    });
});

$('#enviar').click(function() {

    var email = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;

    if ($('#departamento').val() == "@") {
        $('#departamento').focus();
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Debe seleccionar un departamento.'
        });
        return;
    }

    if ($('#ciudad').val() == "@") {
        $('#ciudad').focus();
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Debe seleccionar una ciudad.'
        });
        return;
    }

    if ($('#nombre').val() == "" || $('#nombre').val().length > 50) {
        $('#nombre').focus();
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Debe ingresar un nombre.'
        });
        return;
    }


    if ($('#correo').val() == "" || $('#correo').val().length > 30) {
        $('#correo').focus();
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Debe ingresar un correo.'
        });
        return;
    }
    if (!email.test($('#correo').val().trim())) {
        $('#correo').focus();
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'ingrese un correo valido.'
        });
        return;
    }

    $.ajax({
        data: {
            departamento: $('#departamento').val(),
            ciudad: $('#ciudad').val(),
            nombre: $('#nombre').val(),
            correo: $('#correo').val(),
        },
        dataType: "json",
        type: "POST",
        url: "sendForm.php",
        beforeSend: function() {
            Swal.fire({
                    title: "Enviando información...",
                    text: "Esto tomará unos segundos!",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    imageUrl: "./assets/img/cargando.gif",
                }),
                Swal.disableButtons();
        },
        success: function(response) {
            if (response['estado'] == "ok") {
                Swal.fire({
                    icon: 'success',
                    title: 'Ok!',
                    text: response['mensaje'],
                    showConfirmButton: false,
                    timer: 3000
                }).then(setTimeout('location.reload()', 3000));
            }
            if (response['estado'] == "error") {
                Swal.fire({
                    type: 'error',
                    title: 'Opps!',
                    text: response['mensaje'],
                });
            }
        }
    });

});